# Lag et program som summerer sammen tallene fra 4 til 20.
# Summer partall og oddetall hver for seg.

s_partall = 0
s_oddetall = 0
for i in range(4,21):
    if i % 2 == 0:
        s_partall += i
    else:
        s_oddetall += i
        
print(s_partall + s_oddetall)