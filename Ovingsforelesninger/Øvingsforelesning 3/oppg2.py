# Lag et program som printer alle tallene fra 1 til 10

# Alternativ 1
for i in range(10):
    print(i+1)
    
# Alternativ 2
for i in range(1, 11):
    print(i)