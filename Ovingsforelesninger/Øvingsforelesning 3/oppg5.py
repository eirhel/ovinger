# Lag et program som printer alle tallene 
# fra 1 til 10 ved hjelp av en while-løkke.

i = 1

while i <= 10:
    print(i)
    i += 1
    
for x in range(1,11):
    print(x)