# Lag et program som gjør følgende
# 1) Tar inn et tall fra bruker
# 2) Summerer tallene fra 1 og oppover helt til summen overstiger tallet fra bruker
# 3) Skriver ut hvert regnestykke til skjerm

grenseverdi = int(input("Skriv inn en grense: "))
sum_av_tall = 0
i = 1

while sum_av_tall < grenseverdi:
    gammel_verdi = sum_av_tall
    sum_av_tall += i
    ny_verdi = sum_av_tall
    print(gammel_verdi, "+", i, "=", ny_verdi)
    i += 1
    