# Lag et program som spør brukeren om to tall
# og skriver ut absoluttverdien av differansen

tall1 = int(input("Skriv inn et tall: "))
tall2 = int(input("Skriv inn et nytt tall: "))

# Måte 1
diff = tall1 - tall2
absoluttverdi = abs(diff)
print("Absoluttverdien av", tall1, "og", tall2, "er", absoluttverdi)

# Måte 2 (raskere, mer direkte)
print("Absoluttverdien av", tall1, "og", tall2, "er", abs(tall1-tall2))