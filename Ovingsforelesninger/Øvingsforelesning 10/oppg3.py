#Oppgave 1
def read_file(filename):
    with open(filename, "r") as file:
        return file.readlines()       



#Oppgave 2
#Alternativ 1
def fix_ingredients(streng):
    streng = streng.strip()
    liste = streng.split(", ")
    return liste

#Alternativ 2
def fix_ingredients2(streng):
    return streng.strip().split(", ")



#Oppgave 3
def make_dict(foodlist):
    food_dict = {}
    for element in foodlist:
        element = element.strip().split(": ")
        dish = element[0]
        ingredients = element[1]
        food_dict[dish] = fix_ingredients(ingredients)
    return food_dict


#Oppgave 4
#Alternativ 1
def print_recipe(food_dict, dish):
    if dish in food_dict.keys():
        ingredients = food_dict[dish]
        number = len(ingredients)
        print(dish + " has " + str(number) + " ingredients: " + ", ".join(ingredients))
    else:
        print("No dish called " + dish)
        
#Alternativ 2 
def print_recipe2(food_dict, dish):
    try:
        ingredients = food_dict[dish]
        number = len(ingredients)
        print(dish + " has " + str(number) + " ingredients: " + ", ".join(ingredients))
    except:
        print("No dish called " + dish)
        
        
  
#Oppgave 5
def all_recipes_with(food_dict):
    new_dict = {}
    for dish, ingredients in food_dict.items():
        for ingredient in ingredients:
            if ingredient not in new_dict.keys():
                new_dict[ingredient] = [dish]
            else:
                new_dict[ingredient].append(dish)
    return new_dict



#Oppgave 6
import random

food_list = read_file("food.txt")
food_dict = make_dict(food_list)
recipe_dishes = all_recipes_with(food_dict)
dishes = recipe_dishes["egg"]
dish = random.choice(dishes)
print("Today you'll be eating " + dish)
print_recipe(food_dict, dish)



#Oppgave 7
def beregn_verdi(liste):
    vokaler = 'aeiouyæøå'
    summ = 0
    for element in liste:
        for bokstav in element:
            if bokstav in vokaler:
                summ += 5
            else:
                summ += 1
    return summ

def value_food(food):
    maks = 0
    for key in food.keys():
        verdi = beregn_verdi(food[key])
        if verdi > maks:
            maks = verdi
            mat = key
    print("Den dyreste retten er " + mat + " som koster " + str(maks))


