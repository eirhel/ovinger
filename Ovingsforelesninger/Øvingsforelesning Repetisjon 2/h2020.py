import random

# a)
def new_deck():
    liste = []
    for f in "sdhc":
        for v in range(2,15):
            liste.append((v,f))
    return liste
    
    

# b)
def shuffle(deck):
    n = len(deck)
    for i in range(0, n - 1):
        j = random.randrange(i, n)
        deck[i], deck[j] = deck[j], deck[i] # Swap
    return deck


