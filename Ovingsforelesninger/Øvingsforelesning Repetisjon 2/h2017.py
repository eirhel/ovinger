# Modul som trengs til funksjonen date_diff
import datetime

# Matrise (2D-liste) over bilene som passerer fotoboks A
car_table = [[2017, 11, 17, 6, 21, 12, 'HB69082'],
             [2017, 11, 17, 6, 21, 53, 'CV86023'],
             [2017, 11, 17, 6, 23, 0, 'HD27560'],
             [2017, 11, 17, 6, 23, 2, 'UT29891'],
             [2017, 11, 17, 6, 24, 25, 'IS11293'],
             [2017, 11, 17, 6, 24, 40, 'EL73840'],
             [2017, 11, 17, 6, 24, 41, 'UT55227'],
             [2017, 11, 17, 6, 26, 55, 'NB59108'],
             [2017, 11, 17,6, 27, 29, 'UT46408'],
             [2017, 11, 17, 6, 28, 19, 'LE68228']]

# date_diff er innebygd i oppgaven, og brukes for å regne ut hvor mange dager
# det er mellom to datoer på formatet [y,m,d]
def date_diff(start,end):
    d0 = datetime.date(start[0],start[1],start[2])
    d1 = datetime.date(end[0],end[1],end[2])
    delta = d1-d0
    return delta.days

# 3b)
# Her skal skrive funksjonen time_diff
def time_diff(start, end):
    seconds_start = start[3]*60*60 + start[4]*60 + start[5]
    seconds_end = end[3]*60*60 + end[4]*60 + end[5]
    sec_diff = seconds_end - seconds_start
    
    day_diff = date_diff(start[0:3], end[0:3])
    sec_in_day = 24*60*60
    
    total_diff = sec_diff + day_diff*sec_in_day
    return total_diff

# 3c)
# Her skal du skrive funksjonen check_min_distance
def check_min_distance(car_table,diff):
    crazy_drivers=[]
    for i in range(len(car_table)-1):
        first_car=car_table[i][0:6]
        sec_car=car_table[i+1][0:6]
        if time_diff(first_car,sec_car) < diff:
            crazy_drivers.append(car_table[i+1][6])
    return crazy_drivers

# 3d)
# Her skal du skrive funksjonen list_el_cars
def list_el_cars(car_table):
    el_cars = 0
    for car in car_table:
        plate = car[6]
        if (plate[0:2] == "EL" or plate[0:2] == "EK" or plate[0:2] == "EV"):
            el_cars += 1
    return el_cars

# 3e)
# Her skal du skrive funksjonen generate_license_numbers
import random

def generate_license_numbers(amount):
    letters = ('BS','CV','EL','FY','KU','LE','NB','PC','SY','WC')
    numbers = []
    while len(numbers) < amount:
        plate = random.choice(letters) + str(random.randint(10000,99999))
        if plate not in numbers:
            numbers.append(plate)
    return numbers
