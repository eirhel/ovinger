store = [['torsk', 200],['sei',100]]

# 3b)
def fish_amount(store, kind):
    for fisk in store:
        if fisk[0] == kind:
            return fisk[1]
    return 0

# 3c)
def add_fish(store, fangst):
    funnet = False # Eksisterer fisketypen fra før
    for type in store:
        if fangst[0] == type[0]:
            type[1] = type[1] + fangst[1]
            funnet = True
    if not funnet:
        store.append(fangst)
    return store

