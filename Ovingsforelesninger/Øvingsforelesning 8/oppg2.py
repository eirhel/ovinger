def strange_weather(temp,rain):
    counter = 0
    record = 0
    start = 0
    stop = 0
    tempstart = 0
    N = len(temp)

    for i in range(0,N-1):
        if (temp[i]<0 and (temp[i]>temp[i+1]) and (rain[i]<rain[i+1])):
            counter = counter + 1
            if (counter==1):
                tempstart = i
            if (counter>record):
                record = counter
                start = tempstart
                stop = i+1 # Stops at the next item in the table
        else:
            counter = 0
    if (record == 0):
        return (0,0)
    else:
        return (start+1,stop+1)
    
temp=[1, 3, 4,-5,-6,-7,-8,-9,3,0]
rain=[0,20,30, 0,10,30,50, 0,5,2]
(start, stop) = strange_weather(temp,rain)
print(start, stop)
