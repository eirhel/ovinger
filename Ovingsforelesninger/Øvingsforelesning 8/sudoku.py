A = '006905010970012305020004860503800020000000000080001907054100070207450093060703100'

# Oppgave 1a)
def string_to_matrix(A):
    B = []
    for i in range(9):
        liste = []
        for j in range(9):
            liste.append(int(A[i*9+j]))
        B.append(liste)
    return B

# Oppgave 1b)
def print_board(A):
    print("")
    print("    0 1 2   3 4 5   6 7 8  ")
    print("  +-------+-------+-------+")
    for i in range(9):
        print(i,"|", A[i][0], A[i][1], A[i][2],"|", A[i][3], A[i][4], A[i][5],
              "|", A[i][6], A[i][7], A[i][8], "|")
        if ((i+1) % 3 == 0):
            print("  +-------+-------+-------+")

# Oppgave 1c)
def get_horizontal(A, x):
    B = [0]*9
    for i in range(9):
        B[i] = A[x][i]
    return B

# Oppgave 1d)
def get_square(A, x, y): # x = [0, 1, 2], y = [0, 1, 2]
    B = [0]*9
    for i in range(3):
        for j in range(3):
            B[i*3 + j] = A[y*3 + i][x*3 + j]
    return B

# Oppgave 1e)
def board_to_string(A):
    B = ""
    for i in range(9):
        for j in range(9):
            B += str(A[i][j])
            
    return B

import numpy as np

terningskast = "65242212346251314"
x = np.zeros(len(terningskast))

for i in range(len(x)):
    x[i] = terningskast[i]

print(x)
