# Oppgave 9
def naaverdi(belop, n, rente):
    nverdi = belop / ((1+rente)**n)
    return nverdi

# Oppgave 10
def naaverdi_kontantstrom(kontantstrom, rente):
    nverdi = 0 
    for i in range(len(liste)):
        nverdi += naaverdi(liste[i], i, rente)
    return nverdi

# Oppgave 11
def internrente(kontantstrom):
    rente = 0.05
    while True:
        nv = naaverdi_kontantstrom(kontantstrom, rente)
        if abs(nv) < 10:
            return rente
        
        if rente > 10:
            rente += 0.00001
        else:
            rente -= 0.00001