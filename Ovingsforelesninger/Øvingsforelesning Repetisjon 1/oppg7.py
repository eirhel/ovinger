# Lag en funksjon som tar inn antall kast som parameter. 
# Funksjonen skal lage antall kast tilfeldige tall mellom 1 og 6,
# og returnere summen av tallene som ble laget.

import random

def terning(antall):
    sum_av_kast = 0
    for i in range(antall):
        tall = random.randint(1,6)
        sum_av_kast += tall
    return sum_av_kast

print(terning(10))   # Eksempel på kjøring