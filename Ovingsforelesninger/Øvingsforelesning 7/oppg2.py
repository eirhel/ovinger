grupperom = {
"Gløshaugen": ("G301", "G302", "G303", "KJL4", "KJL21", "KJL22"),
"Kalvskinnet": ("G202", "G204", "G405", "LY4.089", "LY4.086"),
"Dragvoll": ("6278", "6279", "6280"),
"Øya": ("ØHG01", "ØHG05"),
"Gjøvik": ("T118", "T206", "T208", "K110", "K112", "A158"),
"Ålesund": ("A433", "A436", "B432", "B433", "B434", "B435")
}

# a)
# Finn ut hvilket campus grupperom KJL4 tilhører,
# og skriv det ut til skjerm.
for campus, romliste in grupperom.items():
    if "KJL4" in romliste:
       print(campus)

# b)
# Grupperommene KJL4 og B433 er utstyrt med flatskjerm.
# Finn alle campusene som har grupperom med flatskjerm,
# og legg disse til i en liste campus_med_flatskjerm.
# Alternativ 1
campus_med_flatskjerm_b1 = []
for campus, romliste in grupperom.items():
    if "KJL4" in romliste or "B433" in romliste:
       campus_med_flatskjerm_b1.append(campus)

# Alternativ 2
campus_med_flatskjerm_b2 = []
for campus, romliste in grupperom.items():
    for rom in romliste:
        if rom == "KJL4" or rom == "B433":
           campus_med_flatskjerm_b2.append(campus)
        
# c)
# Grupperommene på KJEL-bygget (KJL4, KJL21 og KJL22),
# samt grupperommene B433, B435, G202, G302 og ØHG01 er utstyrt med flatskjerm.
# Finn alle campusene som har grupperom med flatskjerm,
# og legg disse til i en liste campus_med_flatskjerm

# Alternativ 1
rom_med_flatskjerm = ("KJL4", "KJL21", "KJL22", "B433", "B435", "G202", "G302", "ØHG01")
campus_liste = []
for campus, romliste in grupperom.items():
    for rom in romliste:
        if rom in rom_med_flatskjerm:
            campus_liste.append(campus)
            
campus_set = set(campus_liste)
campus_med_flatskjerm_c1 = list(campus_set)

# Alternativ 2
campus_med_flatskjerm_c2 = []
for campus, romliste in grupperom.items():
    for rom in romliste:
        if rom[:3] == "KJL" or rom == "B433" or rom == "B435" or rom == "G202" or rom == "G302" or rom == "ØHG01":
            if campus not in campus_med_flatskjerm_c2:
                campus_med_flatskjerm_c2.append(campus)
                
print(campus_med_flatskjerm_b1)
print(campus_med_flatskjerm_b2)
print(campus_med_flatskjerm_c1)
print(campus_med_flatskjerm_c2)
            