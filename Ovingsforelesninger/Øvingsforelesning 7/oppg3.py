# a)
# Les fra filen studenterTDT4111.txt, og lagre innholdet i en dictionary.
# Nøklene i dictionarien skal være studieprogrammer,
# og verdiene skal være lister med studentene som tilhører disse studieprogrammene.
stats = {}
with open("studenter.txt", "r") as file:
    for line in file.readlines():
        line = line.strip()
        line = line.split(", ")
        navn = line[0]
        studie = line[1]
        if studie in stats.keys():
            stats[studie].append(navn)
        else:
            stats[studie] = [navn]


# b)
# Lag en funksjon reference_group som tar inn dictionaryen students.
# Funksjonen skal returnere en liste med en tilfeldig valgt student
# fra hvert studieprogram.
import random

def reference_group(stats):
    referansestudenter = []
    for studentliste in stats.values():
        random_student = random.choice(studentliste)
        referansestudenter.append(random_student)
    return referansestudenter

reference_group = reference_group(stats)


# c)
# Vi ønsker å skrive  referansegruppestudentene til en fil. 
# Lag en ny fil som du kaller "referansegruppe.txt".
# Merk at linjen under tittelen "Referansegruppe"
# skal være like# lang som ordet "Referansegruppe". 
# Merk også at navnene på studentene i referansegruppen
# skal være sortert i alfabetisk rekkefølge.

with open("referansegruppe.txt", "w") as file:
    l = len("Referansegruppe")
    file.write("Referansegruppe\n")
    s = ""
    for i in range(l):
        s+="-"
    file.write(s+"\n")
    for student in reference_group:
        file.write(student+"\n")

# d)
# Vi ønsker å finne de tre studiene som har flest studenter som tar TDT4111.
# Lag en funksjon top_three som tar inn en dictionary og
# finner de tre studienei dictionaryen med flest studenter.
# Funksjonen skal printe ut disse tre studiene på formatet nedenfor.
# Funksjonen skal ikke returnere noe.

def top_three(stats):
    number_of_students = []
    for navneliste in stats.values():
        number = len(navneliste)
        number_of_students.append(number)
    
    number_of_students.sort()
    number_of_students.reverse()
    number_of_students = number_of_students[:3]
    
    top_three = []
    
    for number in number_of_students:
        for studie, navneliste in stats.items():
            if len(navneliste) == number:     
                top_three.append(studie)
                
    for i in range(len(top_three)):
        print(str(i+1) + ". plass: " + top_three[i])
        
top_three(stats)