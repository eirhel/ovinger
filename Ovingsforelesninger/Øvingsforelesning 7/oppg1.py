# a)
# Lag to sets: et som består av alle tallene i 3-gangen mellom 1 og 100,
# og et som består av alle tallene i 5-gangen mellom 1 og 100.
set3 = set([])
for i in range(3, 101, 3):
    set3.add(i)
    
set5 = set([])
for i in range(5, 101, 5):
    set5.add(i)

# b)
# Finn ut hvor mange tall mellom 1 og 100 som både er delelige på 3 og 5.
set3og5 = set3.intersection(set5)

# c)
# Lag en liste med alle tallene i 5-gangen som ikke er delelige med 3.
# Lista skal være sortert.
set5ikke3 = set5.difference(set3)
liste = list(set5ikke3)
liste.sort()
print(liste)