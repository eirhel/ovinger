# Lag en funksjon som returnerer True hvis tallet er et primtall,
# og False ellers.

# Funksjon som sjekker om et tall (x) er delelig med et annet tall (y)
def delelig(x, y):
    if x % y == 0:
        return True
    else:
        return False
    
def primtall(tall):
    for i in range(2, tall):
        if delelig(tall, i) == True:
            return False
    return True
