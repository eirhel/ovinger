# Lag en funksjon som tar inn 3 tall, og returnerer det største tallet

def storste_tall(x, y, z):
    if (x > y and x > z):
        return x
    elif (y > x and y > z):
        return y
    else:
        return z
    
print(storste_tall(20, 100, 49))