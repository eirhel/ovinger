# Vi skal lage et enkelt terningspill. En runde i spillet går ut på at spilleren
# gjetter summen av antall øyne,før 5 terninger trilles. Hvis han gjetter riktig,
# får han 3 poeng. Hvis han er innenfor 5 fra riktig verdi, får han 1 poeng. 


import random

# Funksjon som returnerer et tilfeldig tall mellom lower og upper
def random_number(lower, upper):
    return random.randint(lower, upper)

# Funksjon som returnerer summen av antall øyne på n terninger
def sum_of_dices(n):
    total = 0
    for i in range(n):
        dice = random_number(1, 6)
        total += dice

    return total


# Lag en funksjon one_round() som ber spilleren gjette summen av antall øyne.
# Funksjonen skal deretter skriver ut hvor mange øyne det ble, og hvor mange poeng
# spilleren fikk. Spillerens poeng skal også returneres.

def one_round():
    guess = int(input("Gjett summen av antall øyne: "))
    antall_oyne = sum_of_dices(5)
    print("Sum av antall øyne:", antall_oyne)
    if guess == antall_oyne:
        poeng = 3
    elif abs(guess - antall_oyne) <= 5:
        poeng = 1
    else:
        poeng = 0
    print("Antall poeng:", poeng)
    return poeng

one_round()
    
    
    
