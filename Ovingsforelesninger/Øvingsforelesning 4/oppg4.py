# Lag en funksjon som tar inn to tall (tall1 og tall2) som parametre.
# Funksjonen skal returnere True dersom tall1 er delelig med tall2, og False ellers.

# Alternativ 1
def delelig(x, y):
    if x % y == 0:
        return True
    else:
        return False
    
# Alternativ 2
def delelig_2(x, y):
    return (x % y == 0)
