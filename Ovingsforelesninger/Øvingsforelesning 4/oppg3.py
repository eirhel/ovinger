# Lag en funksjon som spør bruker hvilket år hun er født i, og om hun har hatt bursdag i år. 
# Ut i fra svarene fra brukeren, skal funksjonen printe hvor gammel brukeren er.

def alder():
    aarstall = int(input("Hvilket år er du født i? "))
    hatt_bursdag = input("Har du hatt bursdag i år? (j/n) ")
    if hatt_bursdag == "j":
        bruker_alder = 2022 - aarstall
    else:
        bruker_alder = 2022 - aarstall - 1
    print("Du er", bruker_alder, "år.")
    
alder() 