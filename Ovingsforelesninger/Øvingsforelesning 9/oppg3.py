import random
import numpy as np
import matplotlib.pyplot as plt

# Grønn
def terninger_gronn(antall_kast):
    stats = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
    for i in range(antall_kast):
        kast = random.randint(1,6)
        stats[kast] += 1
    return stats



# Gul
def terninger_gul(antall_kast):
    stats = {2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7:0, 8:0,9:0,10:0,11:0,12:0}
    for i in range(antall_kast):
        terning1 = random.randint(1,6)
        terning2 = random.randint(1,6)
        summ = terning1 + terning2
        stats[summ] += 1
        
    return stats



# Rød
def plot(stats):
    x = stats.keys()
    y = stats.values()
    plt.bar(x,y)
    plt.show()
    
stats1 = terninger_gul(10)
stats2 = terninger_gul(100)
stats3 = terninger_gul(1000)

plot(stats1)
plot(stats2)
plot(stats3)
    
