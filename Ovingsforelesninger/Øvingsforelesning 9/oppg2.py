import numpy as np
import datetime
import matplotlib.pyplot as plt

# Oppgave 2b)
def get_x_y(filename):    
    with open(filename, "r") as file:
        lines = file.readlines()
        antall_dager = len(lines)
        x = np.arange(1, antall_dager+1)
        y = np.zeros(antall_dager)
        i = 0
        for line in lines:
            line = line.strip()
            line = line.split("\t")
            dato = line[0]
            tilfeller = line[1]
            nye_tilfeller = line[2]
            y[i] = nye_tilfeller
            i += 1  
    return x, y



# Oppgave 2c)
def flest_nye_tilfeller(y):
    max_value = np.max(y)
    max_index = np.argmax(y)
    day1 = datetime.date(2020, 2, 21)
    max_day = day1 + datetime.timedelta(days = int(max_index))
    max_day = str(max_day)
    max_day = max_day.split("-")
    max_day = max_day[2] + "." + max_day[1] + "." + max_day[0]
    print("Dato med flest nye tilfeller: " + max_day)
    print("Antall tilfeller: " + str(int(max_value)))
    
x, y = get_x_y("covid.txt")
#flest_nye_tilfeller(y)



# Oppgave 2d)
def plot(x, y):
    plt.plot(x, y)
    plt.show()
    
plot(x, y)

