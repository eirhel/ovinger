def sort_names(streng):
    navneliste = streng.split()
    for i in range(len(navneliste)):
        navneliste[i] = navneliste[i].strip(',')
    navneliste.sort()
    return navneliste

streng = "Frodo, Sam, Pippin, Merry, Legolas, Gimli, Aragorn, Boromir, Gandalf"

print(sort_names(streng))
        