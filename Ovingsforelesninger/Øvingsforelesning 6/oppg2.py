# Alternativ 1
def sammenligning_av_strenger(s1, s2):
    return s1[:3].lower() == s2[:3].lower()

print(sammenligning_av_strenger("FOTOGRAFI", "Frankrike"))

# Alternativ 2
def sammenligning_av_strenger2(s1, s2):
    s1_ny = s1[:3] # De tre første elementene i s1
    s2_ny = s2[:3] # De tre første elementene i s2
    if s1_ny.lower() == s2_ny.lower():
        return True
    else:
        return False

print(sammenligning_av_strenger2("fotografi", "FOTBALL"))