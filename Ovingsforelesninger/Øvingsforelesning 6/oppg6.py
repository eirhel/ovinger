def short_words(liste):
    s = ""
    for streng in liste:
        streng = str(streng)
        if (streng.isalpha() and len(streng) < 5):
            s += streng
            s += " "
    return s

print(short_words(["pumbaa", "simba", "bb-8", "lilo", "abu", "hir0", 314]))