def indeks_bokstav(streng, bokstav):
    indeks_liste = []
    for i in range(len(streng)):
        if streng[i] == bokstav:
            indeks_liste.append(i)
    return indeks_liste

print(indeks_bokstav("bananpannekaker", 'n'))