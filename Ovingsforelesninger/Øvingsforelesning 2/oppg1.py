# Lag et program som gjør følgende
# 1) Spør bruker om to tall, og lagrer disse som variabler
# 2) Finner ut hvilket av tallene som er minst
# 3) Skriver ut “z er det minste tallet av x og y” til skjerm

tall1 = int(input("Skriv inn et tall: "))
tall2 = int(input("Skriv inn et tall: "))

print(min(tall1, tall2), "er det minste tallet av", tall1, "og", tall2)