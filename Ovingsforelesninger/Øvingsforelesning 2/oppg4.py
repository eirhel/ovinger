# Lag et program som ber bruker om å skrive inn et passord,
# og som sjekker om det er likt et passord som du har lagret i en variabel

passord = "itgk123"
bruker_passord = input("Skriv inn passord: ")

er_passord_riktig = (passord == bruker_passord)

print("Riktig passord:", er_passord_riktig)