# Lag et program som gjør følgende
# - Tar inn to tall, kalt tall1 og tall2
#  - Dersom tallene er like skal programmet skrive “Gratulerer, tallene er like” 
#  - Hvis tall1 er større enn tall2 skal programmet skrive ut
#    “<tall1> er <differanse mellom tallene> større enn <tall2>”
#  - Dersom tall2 er større enn tall1 skal programmet skrive ut
#    ”<tall1> er <differanse mellom tallene> mindre enn <tall2>”.
#  - Til slutt skal programmet skrive ut “Takk for denne gang!”

tall1 = int(input("Skriv inn et tall: "))
tall2 = int(input("Skriv inn et tall: "))

if tall1 == tall2:
    print("Gratulerer, tallene er like")
    
elif tall1 > tall2:
    print(tall1, "er", tall1 - tall2, "større enn", tall2)
    
else:
    print(tall1, "er", abs(tall1 - tall2), "mindre enn", tall2)
    
print("Takk for denne gang")