# Lag et program som tar inn et tall og skriver ut
# "<tall> er et partall" hvis det er et partall

tall = int(input("Skriv inn et tall: "))

if tall % 2 == 0:
    print(tall, "er et partall!")

