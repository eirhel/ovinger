# Karsten vil endre lista si slik at hver tid kun har én desimal.
# Lag en funksjon en_desimal som tar inn en liste med tall,
# og returnerer en liste der hvert tall i lista kun har én desimal. 

tider = [46.98, 47.13, 47.09, 46.58, 47.12]

def en_desimal(liste):
    for i in range(len(liste)):
        liste[i] = round(liste[i], 1)
    return liste

print(en_desimal(tider))
        

