# a) Opprett en liste med tallene fra 1 til 10.
# Alternativ 1
liste_a = []
for i in range(1, 11):
    liste_a.append(i)
    
# Alternativ 2
liste_a2 = [i for i in range(1,11)]

# b) Opprett en liste med elementene i strengen "Det er 79 dager til jul!"
streng = "Det er 79 dager til jul!"
liste_b = list(streng)

# c) Opprett en liste med annethvert tall fra 0 til 50.
liste_c = []
for i in range(0,51,2):
    liste_c.append(i)

# d) Opprett en liste med annenhver True og False med lengde 20.
liste_d = []
for i in range(20):
    if i%2 == 0:
        liste_d.append(True)
    else:
        liste_d.append(False)

