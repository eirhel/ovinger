handleliste = ["havregryn", "melk", "epler", "squash", "tomater", "bamsemums"]

# a) Fjern melk fra handlelista
handleliste.remove("melk")

# b) Sjekk om egg er i handlelista.
#    Hvis ikke, legg til egg i handlelista.
if not "egg" in handleliste:
    handleliste.append("egg")

# c) Sorter handlelista
handleliste.sort()

# d) Fjern det siste elementet i handlelista.
del handleliste[-1]

# e) Reverser handlelista.
handleliste.reverse()

# f) Print handlelista. Hvor mange varer er det i handlelista nå?
print(handleliste)
print(len(handleliste))